# ikelca

## Requisitos

### Plugins
 * custom post type: ./plugins/custom-post-type-ui.1.6.1.zip. Descomprimir en la ../wp-content/plugins/

## Configuraciones
Agregar un menú nuevo
* Menu Name: Menú principal
* Display location: Primary

Agregar nuevo post type: 

Post Type Productos:

 * Post Type Slug: productos
 * Plural Label: Productos
 * Singular Label: Producto
 * Support: Title | Editor | Featured Image | Excerpt
 * Build-in Taxonomies: Categories (WP Core)

 Agregar page de productos
* Title: Productos
* Template: Productos

Post Type Recetas:

 * Post Type Slug: recetas
 * Plural Label: Recetas
 * Singular Label: Receta
 * Support: Title | Editor | Featured Image | Excerpt
 * Build-in Taxonomies: Categories (WP Core)

 Agregar page de recetas
* Title: Recetas
* Template: Recetas