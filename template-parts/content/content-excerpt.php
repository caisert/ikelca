<?php
/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="card">
		<?php 
		the_post_thumbnail( 'post-thumbnail', array( 'class' => 'card-img-top' ) );/* the_post_thumbnail(); */ ?>
		<div class="card-body">
			<?php 
				the_title( sprintf( '<h2 class="card-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
			<p class="card-text">
				<?php 
					the_excerpt(); 
				?>
			</p>
		</div>
		<header class="entry-header">
			<?php
		if ( is_sticky() && is_home() && ! is_paged() ) {
			printf( '<span class="sticky-post">%s</span>', _x( 'Featured', 'post', 'twentynineteen' ) );
		}
		?>
		 </header><!--.entry-header -->
		
		<footer class="entry-footer">
			<?php twentynineteen_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	</div>
</article><!-- #post-${ID} -->
