function ejs_fetch_render(path, id, data, unwrap) {
    fetch(path+'.ejs')
        .then(function (response) {
            return response.text();
        })
        .then(function (res) {
            var element = $('#'+id);
            element.html(ejs.render(res,data))
            element = $('#'+id+' :first');
            if(unwrap != false)
            element.unwrap()
        });
}
function router(){
    var hash = window.location.hash.replace('#','');
    var url_hash = hash.toString().split('/');
    var data = url_hash[1];
    var version_home = data ? 'home'+data : 'home'
    var route = url_hash[0] || version_home;
    ejs_fetch_render(route,'content',data, false)
}
function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

window.onhashchange = function () {
    router()
}
router();


function animateCSS(element, animationName, callback) {
    const nodes = document.querySelectorAll(element)
    console.log(element)
    nodes.forEach(function(node){
        node.classList.add('animated', animationName)
        function handleAnimationEnd() {
            node.classList.remove(animationName)
            node.removeEventListener('animationend', handleAnimationEnd)
            if (typeof callback === 'function') callback()
        }
        node.addEventListener('animationend', handleAnimationEnd)
    })
}
//And use it like this:
// animateCSS('.my-element', 'bounce')
// or
// animateCSS('.my-element', 'bounce', function() {
  // Do something after animation
// })

// var slide ={
//     init: function (slideName,options) {
//         slideName = slideName ? slideName : '.ist-slide'
//         var slide = document.querySelectorAll(slideName)
//         var items, next, prev;
//         slide.forEach((s,i) => {
//             next = s.querySelector('.ist-slide-next');
//             prev = s.querySelector('.ist-slide-prev');
//             items =s.querySelectorAll('.ist-slide-item');
//             items.forEach((it, i)=>{
//                 var delays = options.delays[i];
//                 var callbacks = options.callbacks[i];
//             })
            
//         });
//     }
// }

window.onload = function () {
    
//     console.log("Start animation loaded")
//     animateCSS('.mark img', 'fadeIn', function() {
//         console.log("finish")
//     });
//     setTimeout(function(){ slideNext()},4000);
//     setTimeout(function(){ 
//         animateCSS('.mark .home-producto', 'bounce', function() {
//             console.log("finish")
//         });
//     }, 5000);  
//     setTimeout(function(){ 
//         animateCSS('.mark .home-producto', 'bounce', function() {
//             console.log("finish")
//         });
//     }, 8000);
//     setTimeout(function(){ slideNext()},10800);
//     setTimeout(function(){ 
//         animateCSS('.mark .home-receta', 'bounce', function() {
//             console.log("finish")
//         });
//     }, 8800);      
//     setTimeout(function(){ 
//         animateCSS('.mark .home-receta', 'bounce', function() {
//             console.log("finish")
//         });
//     }, 9800);
//     $('#carouselExampleIndicators11').on('slide.bs.carousel', function () {
//         setTimeout(function(){
//             const selectSlide = document.querySelectorAll('#home2carousel .active');
//             console.log(selectSlide);
//             selectSlide.forEach(
//                 function(slideItem){
//                     console.log(slideItem.id);
//                     if(slideItem.id === "home2first"){
//                         animateCSS('.mark img', 'fadeIn', function() {
//                             console.log("finish")
//                         });
//                     }else if(slideItem.id === "home2second"){
//                         animateCSS('.mark .home-producto', 'bounce', function() {
//                             console.log("finish")
//                         });
//                     }else if(slideItem.id === "home2third"){
//                         animateCSS('.mark .home-receta', 'bounce', function() {
//                             console.log("finish")
//                         });
//                         animateCSS('.mark .home-receta', 'bounce', function() {
//                             console.log("finish")
//                         });
//                     }
//                 }
//             );
//         },1000)
        
//   /*      if(selectSlide.id === 'home2third'){
//             animateCSS('.mark .home-producto', 'bounce', function() {
//                 console.log("finish")
//             });
//         } */
//     })
}

// function slideNext(){
//     $('#carouselExampleIndicators11').delay(3000).carousel('next');
//     console.log('exito');
// }

function showMapTooltip(evt) {
    let tooltip = document.getElementById("tooltip");
    var text = evt.currentTarget.getAttribute("title");
    tooltip.innerHTML = text;
    tooltip.style.display = "block";
    tooltip.style.left = evt.pageX + 10 + 'px';
    tooltip.style.top = evt.pageY + 10 + 'px';
}

function hideMapTooltip() {
    var tooltip = document.getElementById("tooltip");
    tooltip.style.display = "none";
}